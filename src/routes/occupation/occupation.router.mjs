import express from "express";

import { createOccupation, getAllOccupation,deletedOccupation,updatedOccupation} from "./occupation.controller.mjs";

const OccupationRouter = express.Router();

OccupationRouter.get("/", getAllOccupation);
OccupationRouter.post("/",createOccupation);
OccupationRouter.delete("/:id",deletedOccupation)
OccupationRouter.put("/:id",updatedOccupation)
// OccupationRouter.get("/:id",getOccupationById)

export default OccupationRouter;