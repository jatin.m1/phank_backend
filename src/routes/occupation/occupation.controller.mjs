import { sequelize } from "../../services/postgres.mjs";
import Occupation from "../../models/occupation/occupation.postgres.mjs"
import { addOccupation, findAllOccupation,deleteOccupation,updateOccupation} from "../../models/occupation/occupation.model.mjs";

const createOccupation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const occupation = await addOccupation(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        occupation,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllOccupation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const occupations = await findAllOccupation(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: occupations.length,
      data: {
        occupations,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const deletedOccupation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const occupation=await Occupation.findByPk(id);
    if(!occupation){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteOccupation(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedOccupation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const occupation=await Occupation.findByPk(id);
    if(!occupation){
      res.status(404).json({
        message: "Not Found for updating a Occupation with id = " + id,
        customer: "",
        error: "404"
    });
    }else{
  const data=req.body;
      let result = await updateOccupation(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Occupation with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Occupation with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};


export { createOccupation, getAllOccupation,deletedOccupation,updatedOccupation};
