import { sequelize } from "../../services/postgres.mjs";
import Salary from "../../models/salary/salary.postgres.mjs"
import { addSalary, findAllSalary,deleteSalary,updateSalary} from "../../models/salary/salary.model.mjs";

const createSalary = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const Salary = await addSalary(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        Salary,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllSalary = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const Salary = await findAllSalary(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      data: {
        Salary,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const deletedSalary = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const salary=await Salary.findByPk(id);
    if(!salary){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteSalary(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedSalary = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const salary=await Salary.findByPk(id);
    if(!salary){
      res.status(404).json({
        message: "Not Found for updating a Salary with id = " + id,
        customer: "",
        error: "404"
    });
    }else{
  const data=req.body;
      let result = await updateSalary(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Salary with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Salary with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};


export { createSalary, getAllSalary,deletedSalary,updatedSalary};
