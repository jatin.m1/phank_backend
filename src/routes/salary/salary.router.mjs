import express from "express";

import { createSalary, getAllSalary,deletedSalary,updatedSalary} from "./salary.controller.mjs";

const SalaryRouter = express.Router();

SalaryRouter.get("/", getAllSalary);
SalaryRouter.post("/",createSalary);
SalaryRouter.delete("/:id",deletedSalary)
SalaryRouter.put("/:id",updatedSalary)


export default SalaryRouter;