import { sequelize } from "../../services/postgres.mjs";
import Country from "../../models/country/country.postgres.mjs"
import { addCountry,findAllCountry,deleteCountry,updateCountry } from "../../models/country/country.model.mjs";



const createCountry = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
      const bodyData = req.body;
      const country = await addCountry(bodyData, t);
      await t.commit();
  
      res.status(201).json({
        status: "success",
        data: {
          Country,
        },
      });
    } catch (err) {
      await t.rollback();
      console.log("getting error...");
      console.error(err);
      res.status(400).json({
        error: err,
      });
    }
  };
  
  const getAllCountry = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
      const countries = await findAllCountry(t);
      await t.commit();
      res.status(200).json({
        status: "success",
        length:countries.length,
        data: {
          countries,
        },
      });
    } catch (err) {
      await t.rollback();
      console.log("getting error...");
      console.error(err);
      res.status(400).json({
        error: err,
      });
    }
  };
  const deletedCountry = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
      const id= req.params.id;
      const country=await Country.findByPk(id);
      if(!country){
        res.status(404).json({
            message: "Does Not exist a Customer with id = " + id,
            error: "404",
        });
    } else {
        await deleteCountry(id,t);
        res.status(200).json({
            message: "Delete Successfully a Customer with id = " + id,
           
        });
    }
      await t.commit();
    } catch (err) {
      await t.rollback();
      console.log("getting error...");
      console.error(err);
      res.status(400).json({
        error: err,
      });
    }
  };
  
  const updatedCountry = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
      // const employee = await findAllEmployee(t);
      const id= req.params.id;
      const country=await Country.findByPk(id);
      if(!country){
        res.status(404).json({
          message: "Not Found for updating a Country with id = " + id,
          customer: "",
          error: "404"
      });
      }else{
    const data=req.body;
        let result = await updateCountry(id,data,t)
        if(!result) {
          res.status(500).json({
              message: "Error -> Can not update a Country with id = " + req.params.id,
              error: "Can NOT Updated",
          });
      }
      res.status(200).json({
        message: "Update successfully a Country with id = " + id,
        employee: result,
    });
      }
      await t.commit();
    } catch (err) {
      await t.rollback();
      console.log("getting error...");
      console.error(err);
      res.status(400).json({
        error: err,
      });
    }
  };

  export {createCountry,getAllCountry,deletedCountry,updatedCountry}