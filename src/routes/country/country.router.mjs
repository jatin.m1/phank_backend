import express from "express";

import { createCountry, getAllCountry,deletedCountry,updatedCountry} from "./country.controller.mjs";

const countryRouter = express.Router();

countryRouter.get("/", getAllCountry);
countryRouter.post("/",createCountry);
countryRouter.delete("/:id",deletedCountry)
countryRouter.put("/:id",updatedCountry)


export default countryRouter;