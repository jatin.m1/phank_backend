import { sequelize } from "../../services/postgres.mjs";
import { addVmake,findAllVmake ,deleteVmake,updateVmake} from "../../models/vehical/vmake.model.mjs";
import Vmake from "../../models/vehical/vmake.postgres.mjs";

const createVmake = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const vmake = await addVmake(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        vmake,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getVmake = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const vmakes = await findAllVmake(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: vmakes.length,
      data: {
        vmakes,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const deletedVmake = async (req, res, next) => {

  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    console.log(id)
    const vmake=await Vmake.findByPk(id);
    if(!vmake){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteVmake(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedVmake = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    console.log(id)
    const vmake=await Vmake.findByPk(id);
    if(!vmake){
      res.status(404).json({
        message: "Not Found for updating a State with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

      const data=req.body

      let result = await updateVmake(id,data.name,data.color,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a State with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a State with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

export { createVmake, getVmake ,deletedVmake,updatedVmake};
