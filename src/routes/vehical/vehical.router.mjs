import express from "express";

import { createVehical, 
    deletedVehical, 
    getVehical, 
    getVehicalById, 
    updatedVehical ,
    getVehicalByOrganisationId,
    getVehicalByBranchId,
    getVehicalByEmployeeId,
    updatedVehicalByEmployeeId} from "./vehical.controller.mjs";

const vehicalRouter = express.Router();

vehicalRouter.get("/", getVehical);
vehicalRouter.post("/", createVehical);
vehicalRouter.delete("/:id",deletedVehical)
vehicalRouter.put("/:id",updatedVehical)
vehicalRouter.get("/:id",getVehicalById)
vehicalRouter.get("/organisation/:id",getVehicalByOrganisationId)
vehicalRouter.get("/branch/:id",getVehicalByBranchId)
vehicalRouter.get("/employee/:id",getVehicalByEmployeeId)
vehicalRouter.put("/employee/:id",updatedVehicalByEmployeeId)

export default vehicalRouter;