import { sequelize } from "../../services/postgres.mjs";
import Vtype from "../../models/vehical/vtype.postgres.mjs";
import { addVtype,findAllVtype,deleteVtype,updateVtype,vtypeById } from "../../models/vehical/vtype.model.mjs";

const createVtype = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const vtype = await addVtype(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        vtype,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getVtype = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const vtype = await findAllVtype(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: vtype.length,
      data: {
        vtype,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const getVtypeById=async(req,res,next)=>{
  const t = await sequelize.transaction();
  try {
    const id=req.params.body;
    const vtype=await vtypeById(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      length: Vtype.length,
      data: {
        Vtype,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}

const deletedVtype = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const vtype=await Vtype.findByPk(id);
    if(!vtype){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteVtype(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          Vtype: Vtype,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedVtype = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const vtype=await Vtype.findByPk(id);
    if(!Vtype){
      res.status(404).json({
        message: "Not Found for updating a Vtype with id = " + id,
        customer: "",
        error: "404"
    });
    }else{
  const data=req.body;
      let result = await updateVtype(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Vtype with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Vtype with id = " + id,
     
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};


export { createVtype, getVtype ,deletedVtype,updatedVtype,getVtypeById};
