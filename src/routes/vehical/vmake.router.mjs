import express from "express";

import { createVmake, getVmake ,deletedVmake,updatedVmake} from "./vmake.controller.mjs";

const vmakeRouter = express.Router();

vmakeRouter.get("/", getVmake);
vmakeRouter.post("/", createVmake);
vmakeRouter.delete("/:id",deletedVmake)
vmakeRouter.put("/:id",updatedVmake)


export default vmakeRouter;