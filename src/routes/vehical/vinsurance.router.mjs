import express from "express";

import { createVinsurance, getVinsuranceByVehicalId ,deletedVinsurance,updatedVinsurance} from "./vinsurance.controller.mjs";

const vinsuranceRouter = express.Router();

vinsuranceRouter.get("/vehical/:id", getVinsuranceByVehicalId);
vinsuranceRouter.post("/", createVinsurance);
vinsuranceRouter.delete("/:id",deletedVinsurance)
vinsuranceRouter.put("/vehical/:id",updatedVinsurance)


export default vinsuranceRouter;