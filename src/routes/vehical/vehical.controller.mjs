import { sequelize } from "../../services/postgres.mjs";
import { addVehical,
  findAllVehical ,
  deleteVehical,
  updateVehical,
  vehicalById,
  vehicalByOrganisationId,
  vehicalByBranchId,
  vehicalByEmployeeId,
  updateVehicalByEmployeeId} from "../../models/vehical/vehical.model.mjs";
import Vehical from "../../models/vehical/vehical.postgres.mjs";
const createVehical = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const vehical = await addVehical(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        vehical,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getVehical = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const vehicals = await findAllVehical(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: vehicals.length,
      data: {
        vehicals,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getVehicalById=async(req,res,next)=>{
  const t = await sequelize.transaction();
  try {
    const id=req.params.id;
    console.log(id)
    const vehical=await vehicalById(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      length: vehical.length,
      data: {
        vehical,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}
const getVehicalByOrganisationId=async(req,res,next)=>{
  const t = await sequelize.transaction();
  try {
    const id=req.params.id;
    console.log(id);
    const vehical=await vehicalByOrganisationId(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      length: vehical.length,
      data: {
        vehical,
      },
    });
   
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}

const getVehicalByBranchId=async(req,res,next)=>{
  const t = await sequelize.transaction();
  try {
    const id=req.params.id;
    console.log(id);
    const vehical=await vehicalByBranchId(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      length: vehical.length,
      data: {
        vehical,
      },
    });
   
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}

const getVehicalByEmployeeId=async(req,res,next)=>{
  const t = await sequelize.transaction();
  try {
    const id=req.params.id;
    console.log(id);
    const vehical=await vehicalByEmployeeId(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      data: {
        vehical,
      },
    });
   
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}

const deletedVehical = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const vehical=await Vehical.findByPk(id);
    if(!vehical){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteVehical(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          vehical: vehical,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedVehical = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const vehical=await Vehical.findByPk(id);
    if(!vehical){
      res.status(404).json({
        message: "Not Found for updating a vehical with id = " + id,
        customer: "",
        error: "404"
    });
    }else{
  const data=req.body;
      let result = await updateVehical(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a vehical with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a vehical with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedVehicalByEmployeeId = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {

    const id= req.params.id;
    const data=req.body
    const vehical=await updateVehicalByEmployeeId(id,data,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      data: {
        vehical
      },
    });
   

  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

export { createVehical,
   getVehical ,
   deletedVehical,
   updatedVehical,
   getVehicalById,
   getVehicalByOrganisationId,
   getVehicalByBranchId,
   getVehicalByEmployeeId,
   updatedVehicalByEmployeeId};
