import express from "express";

import { createVtype, getVtype } from "./vtype.controller.mjs";

const vtypeRouter = express.Router();

vtypeRouter.get("/", getVtype);
vtypeRouter.post("/", createVtype);

export default vtypeRouter;