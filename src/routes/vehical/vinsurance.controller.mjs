import { sequelize } from "../../services/postgres.mjs";
import { addVinsurance,findVinsuranceByVehicalId ,deleteVinsurance,updateVinsurance} from "../../models/vehical/vinsurance.model.mjs";
import Vinsurance from "../../models/vehical/vinsurance.postgres.mjs";

const createVinsurance = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const vinsurance = await addVinsurance(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        vinsurance,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getVinsuranceByVehicalId = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
      const id=req.params.id
    const vinsurances = await findVinsuranceByVehicalId(id,t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: vinsurances.length,
      data: {
        vinsurances,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const deletedVinsurance = async (req, res, next) => {

  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    console.log(id)
    const vinsurance=await Vinsurance.findByPk(id);
    if(!vinsurance){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteVinsurance(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedVinsurance = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const data=req.body
    console.log(id)
    const result =await updateVinsurance(id,data,t);
    await t.commit();
    res.status(200).json({
        message: "updated  Successfully "
        
    });
}
catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

export { createVinsurance, getVinsuranceByVehicalId,deletedVinsurance,updatedVinsurance};
