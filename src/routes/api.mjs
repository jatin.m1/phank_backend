
import express from "express";

import statusRouter from "./status/status.router.mjs";
import bankRouter from "./paymentMode/bank.router.mjs"
import paymentModeRouter from './paymentMode/paymentMode.router.mjs'
import relationshipRouter from "./employee/relationship.router.mjs";
import occupationRouter from "./employee/occupation.router.mjs";
import vtypeRouter from "./vehical/vtype.router.mjs";
import vmakeRouter from "./vehical/vmake.router.mjs";
import employeeRouter from "./employee/employee.router.mjs";
import etypeRouter from "./employee/etype.router.mjs";
import vehicalRouter from "./vehical/vehical.router.mjs";
import edesignationRouter from "./employee/edesignation.router.mjs";
import edepartmentRouter from "./employee/edepartment.router.mjs";
import branchRouter from "./branch/branch.router.mjs";
import stateRouter from "./state/state.router.mjs";
import OrganisationRouter from "./organisation/organisation.router.mjs";
import "../associations/index.mjs";
import CountryRouter from "./country/country.router.mjs";
import genderRouter from "./employee/gender.router.mjs";
import managerRouter from "./employee/manager.router.mjs";
import EworkhistoryRouter from "./employee/eworkHistory.router.mjs";
import vinsuranceRouter from "./vehical/vinsurance.router.mjs";
import emetaDataRouter from "./employee/emetaData.router.mjs";
import egradeRouter from "./employee/egrade.router.mjs";
import SalaryRouter from "./salary/salary.router.mjs"
import LeaveRouter from "./employee/leave.router.mjs";
import eeducationRouter from "./employee/eeducation.router.mjs";
import LevelRouter from "./employee/level.router.mjs";
import QualificationRouter from "./employee/qualification.router.mjs";
import efamilyRouter from "./employee/efamily.router.mjs";
import OccupationRouter from "./occupation/occupation.router.mjs";
import EbankDetailsRouter from "./paymentMode/ebankDetails.router.mjs";

const api = express.Router();

api.get("/", (req, res) => res.send("<h1>welcome to auth service!</h1>"));

// 1) status
api.use("/status", statusRouter);          
// 2)bank
api.use("/banks",bankRouter)                 //tested
// 3)paymentMOde
api.use("/paymentmodes",paymentModeRouter)   //tested
// 4)relationship
api.use("/relationship",relationshipRouter)   //tested
// 5)occupation
api.use("/occupation",occupationRouter)       //tested
// 6)vtype
api.use("/vtype",vtypeRouter)                 //tested
// 7)vmake
api.use("/vmake",vmakeRouter)                 //tested
// 8)employee
api.use("/employee",employeeRouter)          //tested
// 9)etype
api.use("/etype",etypeRouter)                //tested
// 10)vehical
api.use("/vehical",vehicalRouter)                //tested
// 11)CURD designation
api.use("/edesignation",edesignationRouter)
// 12) department CURD
api.use("/edepartment",edepartmentRouter)
// 13) branch CURD
api.use("/branch",branchRouter)
// 14)STATE CURD
api.use("/state",stateRouter)
// 15)organisation
api.use("/organisation",OrganisationRouter)
// 16)country
api.use("/country",CountryRouter)
// 17)gender
api.use("/gender",genderRouter)
// 18)MANAGER
api.use("/manager",managerRouter)
// 19)workhistory
api.use("/eworkhistory",EworkhistoryRouter)
// 20)vinsurance
api.use("/vinsurance",vinsuranceRouter)
// 21) metaData
api.use("/metadata",emetaDataRouter)
// 22)egrade
api.use("/egrade",egradeRouter)
// 23)salary
api.use("/salary",SalaryRouter)
// 24)leave
api.use("/leave",LeaveRouter)
// 25)eeducation
api.use("/education",eeducationRouter)
// 26)level
api.use("/level",LevelRouter)
// 27)qualification
api.use("/qualification",QualificationRouter)
// 28)efamily
api.use("/family",efamilyRouter)
// 29)occupation
api.use("/occupation",OccupationRouter)
// 30)bankdetails
api.use("/bankdetails",EbankDetailsRouter)


export default api;

