import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Edesignation from "../../models/employee/edesignation.postgres.mjs"
import { addEdesignation, findAllEdesignation,deleteEdesignation,updateEdesignation} from "../../models/employee/edesignation.model.mjs";

const createEdesignation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const status = await addEdesignation(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllEdesignation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const status = await findAllEdesignation(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: status.length,
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};  

const deletedEdesignation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const edesignation=await Edesignation.findByPk(id);
    if(!edesignation){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteEdesignation(id,t);
      res.status(204).json({
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedEdesignation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const edesignation=await Edesignation.findByPk(id);
    if(!edesignation){
      res.status(404).json({
        message: "Not Found for updating a designation with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateEdesignation(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a edesignation with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createEdesignation , getAllEdesignation,deletedEdesignation,updatedEdesignation};
