import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import { addOccupation ,findAllOccupation} from "../../models/employee/occupation.model.mjs";

const createOccupation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const occupation = await addOccupation(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        occupation ,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllOccupation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const occupation = await findAllOccupation(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: occupation.length,
      data: {
        occupation,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createOccupation, getAllOccupation};
