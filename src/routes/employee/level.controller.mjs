import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Level from "../../models/employee/level.postgres.mjs";
import { addLevel ,findAllLevel,deleteLevel,updateLevel} from "../../models/employee/level.model.mjs";

const createLevel = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const level = await addLevel(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        level,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllLevel = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const levels = await findAllLevel(t);
    await t.commit();
    res.status(200).json({
      status: "success",

      data: {
        levels,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const deletedLevel = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const level=await Level.findByPk(id);
    if(!level){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteLevel(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedLevel = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const level=await Level.findByPk(id);
    if(!level){
      res.status(404).json({
        message: "Not Found for updating a type with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateLevel(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Level with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
      
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createLevel, getAllLevel,deletedLevel,updatedLevel};
