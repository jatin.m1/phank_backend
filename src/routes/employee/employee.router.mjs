import express from "express";

import { createEmployee, getAllEmployee,updatedEmployee,deletedEmployee, getEmployeeById,getEmployeeByDepartmentId,getEmployeeByBranchId} from "./employee.controller.mjs";

const employeeRouter = express.Router();

employeeRouter.get("/", getAllEmployee);
employeeRouter.post("/",createEmployee);
employeeRouter.put("/:id",updatedEmployee)
employeeRouter.delete("/:id",deletedEmployee)
employeeRouter.get("/:id",getEmployeeById)
employeeRouter.get("/department/:id",getEmployeeByDepartmentId)
employeeRouter.get("/branch/:id",getEmployeeByBranchId)

export default employeeRouter;