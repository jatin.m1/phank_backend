import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Manager from "../../models/employee/manager.postgres.mjs";
import { addManager ,findAllManager,deleteManager,updateManager} from "../../models/employee/manager.model.mjs";

const createManager = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const Manager = await addManager(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        Manager,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllManager = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const managers = await findAllManager(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length:managers.length,
      data: {
        managers,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const deletedManager = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const manager=await Manager.findByPk(id);
    if(!manager){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteManager(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedManager = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const manager=await Manager.findByPk(id);
    if(!manager){
      res.status(404).json({
        message: "Not Found for updating a type with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateManager(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Manager with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
      
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createManager, getAllManager,deletedManager,updatedManager};
