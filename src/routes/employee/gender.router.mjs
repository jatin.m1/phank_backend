import express from "express";

import { createGender, getAllGender,updatedGender,deletedGender} from "./gender.controller.mjs";

const genderRouter = express.Router();

genderRouter.get("/", getAllGender);
genderRouter.post("/",createGender);
genderRouter.put("/:id",updatedGender)
genderRouter.delete("/:id",deletedGender)


export default genderRouter;