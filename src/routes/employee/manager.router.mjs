import express from "express";

import { createManager, getAllManager,updatedManager,deletedManager} from "./manager.controller.mjs";

const managerRouter = express.Router();

managerRouter.get("/", getAllManager);
managerRouter.post("/",createManager);
managerRouter.put("/:id",updatedManager)
managerRouter.delete("/:id",deletedManager)


export default managerRouter;