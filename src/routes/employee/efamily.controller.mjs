import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Efamily from "../../models/employee/efamily.postgres.mjs"
import { addEfamily, findEfamilyByEmployeeId,deleteEfamily,updateEfamily} from "../../models/employee/efamily.model.mjs";

const createEfamily = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const status = await addEfamily(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getEfamilyByEmployeeId = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
      const id=req.params.id;
      const name=req.query.name;
      const emg=req.query.emergency;
      console.log(name)
      
    const family = await findEfamilyByEmployeeId(id,name,emg,t);
    await t.commit();
    res.status(200).json({
      status: "success",
     
      data: {
        family,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};  

const deletedEfamily = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const name=req.query.name
    const efamily=await Efamily.findAll({where:{
        employeeId:id
    }});
    if(!efamily){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteEfamily(id,name,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          efamily: efamily,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback(); 
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedEfamily = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const name=req.query.name
    const efamily=await Efamily.findOne({where:{employeeId:id}});
    if(!efamily){
      res.status(404).json({
        message: "Not Found for updating a family with id = " + id,
        customer: "",
        error: "404"
    });
    }else{
  const data=req.body;
      let result = await updateEfamily(id,data,name,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a efamily with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createEfamily , getEfamilyByEmployeeId,deletedEfamily,updatedEfamily};
