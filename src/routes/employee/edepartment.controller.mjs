import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Edepartment from '../../models/employee/edepartment.postgres.mjs'
import { addEdepartment, findAllEdepartment,deleteEdepartment,updateEdepartment,findEdepartmentById} from "../../models/employee/edepartment.model.mjs";

const createEdepartment = async (req, res, next) => {
  const t = await sequelize.transaction();
  try { 
    const bodyData = req.body;
    const status = await addEdepartment(bodyData, t);
    await t.commit();
 
    res.status(201).json({
      status: "success",
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllEdepartment = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const status = await findAllEdepartment(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: status.length,
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getEdepartmentById = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    
    const id= req.params.id;
    const status = await findEdepartmentById(id,t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: status.length,
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const deletedDepartment = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const edepartment=await Edepartment.findByPk(id);
    if(!edepartment){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteEdepartment(id,t);
      res.status(204).json({
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedDepartment = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const edepartment=await Edepartment.findByPk(id);
    if(!edepartment){
      res.status(404).json({
        message: "Not Found for updating a department with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateEdepartment(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a edepartment with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};


export { createEdepartment, getAllEdepartment,deletedDepartment,updatedDepartment ,getEdepartmentById};
