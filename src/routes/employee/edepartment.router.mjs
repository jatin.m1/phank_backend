import express from "express";

import { createEdepartment, getAllEdepartment,deletedDepartment,updatedDepartment,getEdepartmentById} from "./edepartment.controller.mjs";

const edepartmentRouter = express.Router();

edepartmentRouter.get("/", getAllEdepartment);
edepartmentRouter.post("/",  createEdepartment);
edepartmentRouter.put("/:id",updatedDepartment);
edepartmentRouter.delete("/:id",deletedDepartment)
edepartmentRouter.get("/:id",getEdepartmentById)
export default edepartmentRouter; 