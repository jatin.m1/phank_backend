import express from "express";

import {createEdesignation , getAllEdesignation,deletedEdesignation,updatedEdesignation} from "./edesignation.controller.mjs";

const edesignationRouter = express.Router();

edesignationRouter.get("/", getAllEdesignation);
edesignationRouter.post("/",createEdesignation);
edesignationRouter.put("/:id",updatedEdesignation)
edesignationRouter.delete("/:id",deletedEdesignation)

export default edesignationRouter;