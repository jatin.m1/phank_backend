import express from "express";

import {createEmetaData , getAllEmetaData,deletedEmetaData,updatedEmetaData} from "./emetaData.controller.mjs";

const emetaDataRouter = express.Router();

emetaDataRouter.get("/employee/:id", getAllEmetaData);
emetaDataRouter.post("/",createEmetaData);
emetaDataRouter.put("/employee/:id",updatedEmetaData)
emetaDataRouter.delete("/employee/:id",deletedEmetaData)

export default emetaDataRouter;