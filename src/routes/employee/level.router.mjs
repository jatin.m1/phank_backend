import express from "express";

import { createLevel, getAllLevel,updatedLevel,deletedLevel} from "./level.controller.mjs";

const LevelRouter = express.Router();

LevelRouter.get("/", getAllLevel);
LevelRouter.post("/",createLevel);
LevelRouter.put("/:id",updatedLevel)
LevelRouter.delete("/:id",deletedLevel)


export default LevelRouter;