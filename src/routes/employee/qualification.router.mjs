import express from "express";

import { createQualification, getAllQualification,updatedQualification,deletedQualification} from "./qualification.controller.mjs";

const QualificationRouter = express.Router();

QualificationRouter.get("/", getAllQualification);
QualificationRouter.post("/",createQualification);
QualificationRouter.put("/:id",updatedQualification)
QualificationRouter.delete("/:id",deletedQualification)


export default QualificationRouter;