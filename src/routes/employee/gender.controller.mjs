import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Gender from "../../models/employee/gender.postgres.mjs";
import { addGender ,findAllGender,deleteGender,updateGender} from "../../models/employee/gender.model.mjs";

const createGender = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const gender = await addGender(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        gender,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllGender = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const genders = await findAllGender(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length:genders.length,
      data: {
        genders,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const deletedGender = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const gender=await Gender.findByPk(id);
    if(!gender){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteGender(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedGender = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const gender=await Gender.findByPk(id);
    if(!gender){
      res.status(404).json({
        message: "Not Found for updating a type with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateGender(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Gender with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
      
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createGender, getAllGender,deletedGender,updatedGender};
