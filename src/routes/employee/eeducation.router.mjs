import express from "express";

import {createEeducation , getEeducationByEmployeeId,deletedEeducation,updatedEeducation} from "./eeducation.controller.mjs";

const eeducationRouter = express.Router();

eeducationRouter.get("/employee/:id", getEeducationByEmployeeId);
eeducationRouter.post("/",createEeducation);
eeducationRouter.put("/employee/:id",updatedEeducation)
eeducationRouter.delete("/employee/:id",deletedEeducation)

export default eeducationRouter;