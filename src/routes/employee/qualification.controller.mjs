import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Qualification from "../../models/employee/qualification.postgres.mjs";
import { addQualification ,findAllQualification,deleteQualification,updateQualification} from "../../models/employee/qualification.model.mjs";

const createQualification = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const qualification = await addQualification(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        qualification,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllQualification = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const qualifications = await findAllQualification(t);
    await t.commit();
    res.status(200).json({
      status: "success",

      data: {
        qualifications,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const deletedQualification = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const qualification=await Qualification.findByPk(id);
    if(!qualification){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteQualification(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedQualification = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const qualification=await Qualification.findByPk(id);
    if(!qualification){
      res.status(404).json({
        message: "Not Found for updating a type with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateQualification(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Qualification with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
      
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createQualification, getAllQualification,deletedQualification,updatedQualification};
