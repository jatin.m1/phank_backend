import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Leave from "../../models/employee/leave.postgres.mjs";
import { addLeave ,findAllLeave,deleteLeave,updateLeave} from "../../models/employee/leave.model.mjs";

const createLeave = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const leave = await addLeave(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        leave,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllLeave = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const leaves = await findAllLeave(t);
    await t.commit();
    res.status(200).json({
      status: "success",

      data: {
        leaves,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const deletedLeave = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const leave=await Leave.findByPk(id);
    if(!leave){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteLeave(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedLeave = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const leave=await Leave.findByPk(id);
    if(!leave){
      res.status(404).json({
        message: "Not Found for updating a type with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateLeave(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Leave with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
      
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createLeave, getAllLeave,deletedLeave,updatedLeave};
