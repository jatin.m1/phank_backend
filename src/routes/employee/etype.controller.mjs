import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Etype from "../../models/employee/etypes.postgres.mjs";
import { addEtype ,findAllEtype,deleteEtype,updateEtype} from "../../models/employee/etypes.model.mjs";

const createEtype = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const etype = await addEtype(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        etype,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllEtype = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const etypes = await findAllEtype(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: etypes.length,
      data: {
        etypes,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const deletedEtype = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const etype=await Etype.findByPk(id);
    if(!etype){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteEtype(id,t);
      res.status(204).json({
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedEtype = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const etype=await Etype.findByPk(id);
    if(!etype){
      res.status(404).json({
        message: "Not Found for updating a type with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateEtype(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a etype with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
      
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createEtype, getAllEtype,deletedEtype,updatedEtype};
