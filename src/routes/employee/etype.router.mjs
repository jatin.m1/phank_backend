import express from "express";

import { createEtype, getAllEtype,deletedEtype,updatedEtype} from "./etype.controller.mjs";

const etypeRouter = express.Router();

etypeRouter.get("/", getAllEtype);
etypeRouter.post("/",createEtype);
etypeRouter.delete("/:id",deletedEtype)
etypeRouter.put("/:id",updatedEtype)

export default etypeRouter;