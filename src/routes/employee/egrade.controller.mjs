import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Egrade from "../../models/employee/egrade.postgres.mjs"
import { addEgrade, findAllEgrade,deleteEgrade,updateEgrade} from "../../models/employee/egrade.model.mjs";

const createEgrade = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const status = await addEgrade(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllEgrade = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const status = await findAllEgrade(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: status.length,
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};  

const deletedEgrade = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const egrade=await Egrade.findByPk(id);
    if(!egrade){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteEgrade(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          egrade: egrade,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedEgrade = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const egrade=await Egrade.findByPk(id);
    if(!egrade){
      res.status(404).json({
        message: "Not Found for updating a grade with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateEgrade(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a egrade with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createEgrade , getAllEgrade,deletedEgrade,updatedEgrade};
