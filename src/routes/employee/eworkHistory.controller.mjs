import { sequelize } from "../../services/postgres.mjs";
import Eworkhistory from "../../models/employee/eworkHistory.postgres.mjs";
import { addEworkhistory ,findAllEworkhistory,deleteEworkhistory,updateEworkhistory,findEworkhistoryByEmployeeId} from "../../models/employee/eworkHistory.model.mjs";

const createEworkhistory = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const eworkHistory = await addEworkhistory(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        eworkHistory,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllEworkhistory = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const eworkHistorys = await findAllEworkhistory(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: eworkHistorys.length,
      data: {
        eworkHistorys,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const getEworkhistoryByEmployeeId = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
        const id=req.params.id;
      const eworkHistory = await findEworkhistoryByEmployeeId(id,t);
      await t.commit();
      res.status(200).json({
        status: "success",
        data: {
          eworkHistory,
        },
      });
    } catch (err) {
      await t.rollback();
      console.log("getting error...");
      console.error(err);
      res.status(400).json({
        error: err,
      });
    }
  };
const deletedEworkhistory = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
 
    let result = await deleteEworkhistory(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
    });

  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedEworkhistory = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    
    const id= req.params.id;
    const data=req.body
    let result = await updateEworkhistory(id,data,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      data: {
        result
      },
    });


  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createEworkhistory, getAllEworkhistory,deletedEworkhistory,updatedEworkhistory,getEworkhistoryByEmployeeId};
// if(!eworkHistory){
//     res.status(404).json({
//       message: "Not Found for updating a type with id = " + id,
//       customer: "",
//       error: "404"
//   });
//   }else{
// const data=req.body;
//     let result = await updateEworkhistory(id,data,t)
//     if(!result) {
//       res.status(500).json({
//           message: "Error -> Can not update a Eworkhistory with id = " + req.params.id,
//           error: "Can NOT Updated",
//       });
//   }
//   res.status(200).json({
//     message: "Update successfully a Customer with id = " + id,
    
// });
//   }