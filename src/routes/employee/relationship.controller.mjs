import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import { addRelationship ,findAllRelationship} from "../../models/employee/relationship.model.mjs";

const createRelationship = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const relationships = await addRelationship(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        relationships ,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllRelationship = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const allRelations = await findAllRelationship(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: allRelations.length,
      data: {
        allRelations,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createRelationship, getAllRelationship};
