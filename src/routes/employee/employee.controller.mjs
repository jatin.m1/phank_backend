import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Employee from "../../models/employee/employe.postgres.mjs";
import { addEmployee,
   findAllEmployee,
   deleteEmployee,
   updateEmployee,
   employeeById,
   employeeByDepartmentId,
  employeeByBranchId} from "../../models/employee/employee.model.mjs";

const createEmployee = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const employee = await addEmployee(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        employee,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllEmployee = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const employee = await findAllEmployee(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: employee.length,
      data: {
        employee,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedEmployee = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const employee=await Employee.findByPk(id);
    if(!employee){
      res.status(404).json({
        message: "Not Found for updating a customer with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateEmployee(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a customer with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
     
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const deletedEmployee = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const employee=await Employee.findByPk(id);
    if(!employee){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteEmployee(id,t);
      res.status(204).json({
        //204 status code for delete operation
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const getEmployeeById=async(req,res,next)=>{
  const t = await sequelize.transaction();
  try {
    const id=req.params.id;
    const employee=await employeeById(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      length: employee.length,
      data: {
        employee,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}
const getEmployeeByDepartmentId=async(req,res,next)=>{
  const t = await sequelize.transaction();
  try {
    const id=req.params.id;
    const employee=await employeeByDepartmentId(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      length: employee.length,
      data: {
        employee,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}

const getEmployeeByBranchId=async(req,res,next)=>{
  const t = await sequelize.transaction();
  try {
    const id=req.params.id;
    console.log(id)
    const employee=await employeeByBranchId(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      length: employee.length,
      data: {
        employee,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}


export { createEmployee, getAllEmployee,updatedEmployee,deletedEmployee,getEmployeeById,getEmployeeByDepartmentId,getEmployeeByBranchId};
