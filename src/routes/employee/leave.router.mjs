import express from "express";

import { createLeave, getAllLeave,updatedLeave,deletedLeave} from "./leave.controller.mjs";

const LeaveRouter = express.Router();

LeaveRouter.get("/", getAllLeave);
LeaveRouter.post("/",createLeave);
LeaveRouter.put("/:id",updatedLeave)
LeaveRouter.delete("/:id",deletedLeave)


export default LeaveRouter;