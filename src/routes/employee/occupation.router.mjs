import express from "express";

import { createOccupation, getAllOccupation} from "./occupation.controller.mjs";

const occupationRouter = express.Router();

occupationRouter.get("/", getAllOccupation);
occupationRouter.post("/",createOccupation);

export default occupationRouter;