import express from "express";

import {createEgrade , getAllEgrade,deletedEgrade,updatedEgrade} from "./egrade.controller.mjs";

const egradeRouter = express.Router();

egradeRouter.get("/", getAllEgrade);
egradeRouter.post("/",createEgrade);
egradeRouter.put("/:id",updatedEgrade)
egradeRouter.delete("/:id",deletedEgrade)

export default egradeRouter;