import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import Eeducation from "../../models/employee/eeducation.postgres.mjs"
import { addEeducation, findEeducationByEmployeeId,deleteEeducation,updateEeducation} from "../../models/employee/eeducation.model.mjs";

const createEeducation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const status = await addEeducation(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getEeducationByEmployeeId = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
      const id=req.params.id;
    const education = await findEeducationByEmployeeId(id,t);
    await t.commit();
    res.status(200).json({
      status: "success",
     
      data: {
        education,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};  

const deletedEeducation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const eeducation=await Eeducation.findOne({where:{
        employeeId:id
    }});
    if(!eeducation){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteEeducation(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          eeducation: eeducation,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedEeducation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const eeducation=await Eeducation.findOne({where:{employeeId:id}});
    if(!eeducation){
      res.status(404).json({
        message: "Not Found for updating a education with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

  const data=req.body;
      let result = await updateEeducation(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a eeducation with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Customer with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createEeducation , getEeducationByEmployeeId,deletedEeducation,updatedEeducation};
