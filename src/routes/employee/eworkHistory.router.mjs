import express from "express";

import { createEworkhistory, getAllEworkhistory,deletedEworkhistory,updatedEworkhistory,getEworkhistoryByEmployeeId} from "./eworkHistory.controller.mjs";

const EworkhistoryRouter = express.Router();

EworkhistoryRouter.get("/", getAllEworkhistory);
EworkhistoryRouter.post("/",createEworkhistory);
EworkhistoryRouter.delete("/employee/:id",deletedEworkhistory)
EworkhistoryRouter.put("/employee/:id",updatedEworkhistory)
EworkhistoryRouter.get("/employee/:id",getEworkhistoryByEmployeeId)

export default EworkhistoryRouter;