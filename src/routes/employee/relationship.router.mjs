import express from "express";

import { createRelationship, getAllRelationship} from "./relationship.controller.mjs";

const relationshipRouter = express.Router();

relationshipRouter.get("/", getAllRelationship);
relationshipRouter.post("/",createRelationship);

export default relationshipRouter;