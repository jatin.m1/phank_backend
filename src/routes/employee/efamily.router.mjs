import express from "express";

import {createEfamily , getEfamilyByEmployeeId,deletedEfamily,updatedEfamily} from "./efamily.controller.mjs";

const efamilyRouter = express.Router();

efamilyRouter.get("/employee/:id", getEfamilyByEmployeeId);
efamilyRouter.post("/",createEfamily);
efamilyRouter.put("/employee/:id",updatedEfamily)
efamilyRouter.delete("/employee/:id",deletedEfamily)

export default efamilyRouter;