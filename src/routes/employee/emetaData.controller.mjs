import { sequelize } from "../../services/postgres.mjs";
import { addEmetaData ,findAllEmetaData,deleteEmetaData,updateEmetaData} from "../../models/employee/emetaData.model.mjs";

const createEmetaData = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const emetaData = await addEmetaData(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        emetaData,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllEmetaData = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
      const id=req.params.id
    const emetaData = await findAllEmetaData(id,t);
    await t.commit();
    res.status(200).json({
      status: "success",
      
      data: {
        emetaData,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const deletedEmetaData = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
 
    let result = await deleteEmetaData(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
    });

  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedEmetaData = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    
    const id= req.params.id;
    const data=req.body
    console.log(data)
    let result = await updateEmetaData(id,data,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      data: {
        result
      },
    });


  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { createEmetaData, getAllEmetaData,deletedEmetaData,updatedEmetaData};
