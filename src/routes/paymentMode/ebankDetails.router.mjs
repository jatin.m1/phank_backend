import express from "express";

import { createEbankDetails, getAllEbankDetails,deletedEbankDetails,updatedEbankDetails} from "./ebankDetails.controller.mjs";

const EbankDetailsRouter = express.Router();

EbankDetailsRouter.get("/employee/:id", getAllEbankDetails);
EbankDetailsRouter.post("/",createEbankDetails);
EbankDetailsRouter.delete("/employee/:id",deletedEbankDetails)
EbankDetailsRouter.put("/employee/:id",updatedEbankDetails)
// EbankDetailsRouter.get("/:id",getEbankDetailsById)

export default EbankDetailsRouter;