import { sequelize } from "../../services/postgres.mjs";
import EbankDetails from "../../models/paymentMode/ebankDetails.postgres.mjs"
import { addEbankDetails, findAllEbankDetails,deleteEbankDetails,updateEbankDetails} from "../../models/paymentMode/ebankDetails.model.mjs";

const createEbankDetails = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const EbankDetails = await addEbankDetails(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        EbankDetails,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllEbankDetails = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
      const id=req.params.id
      const defaultValue=req.query.default
      // const name = req.query
      console.log(id)
      console.log(defaultValue)
      
    const EbankDetails = await findAllEbankDetails(id,defaultValue,t);
    await t.commit();
    res.status(200).json({
      status: "success",
      // length: EbankDetailss.length,
      data: {
        EbankDetails,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};


const deletedEbankDetails = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const ebankDetails=await EbankDetails.findOne({where:{employeeId:id}});
    if(!ebankDetails){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteEbankDetails(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedEbankDetails = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const ebankDetails=await EbankDetails.findOne({where:{employeeId:id}});
    if(!ebankDetails){
      res.status(404).json({
        message: "Not Found for updating a EbankDetails with id = " + id,
        customer: "",
        error: "404"
    });
    }else{
  const data=req.body;
      let result = await updateEbankDetails(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a EbankDetails with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a EbankDetails with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};


export { createEbankDetails, getAllEbankDetails,deletedEbankDetails,updatedEbankDetails};
