import { sequelize } from "../../services/postgres.mjs";
// import { addTest, findAllTest } from "../../models/tests/tests.model.mjs";
import {createBank ,findAllBank} from "../../models/paymentMode/bank.model.mjs";

const addBank = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const bank = await createBank(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        bank,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllBank = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const banks = await findAllBank(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: banks.length,
      data: {
        banks,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};



export { addBank, getAllBank};
