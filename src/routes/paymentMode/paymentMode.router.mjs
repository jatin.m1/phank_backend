import express from "express";

import { createPaymentModes, getAllPaymentModes,deletedPaymentModes,updatedPaymentModes} from "./paymentMode.controller.mjs";

const paymentModeRouter = express.Router();

paymentModeRouter.get("/", getAllPaymentModes);
paymentModeRouter.post("/",createPaymentModes);
paymentModeRouter.put("/:id",updatedPaymentModes);
paymentModeRouter.delete("/:id",deletedPaymentModes)

export default paymentModeRouter;