import { sequelize } from "../../services/postgres.mjs";
import PaymentModes from "../../models/paymentMode/paymentModes.postgres.mjs"
import { addPaymentModes,findAllPaymentModes,deletePaymentModes,updatePaymentModes } from "../../models/paymentMode/paymentModes.model.mjs";



const createPaymentModes = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
      const bodyData = req.body;
      const paymentModes = await addPaymentModes(bodyData, t);
      await t.commit();
  
      res.status(201).json({
        status: "success",
        data: {
          paymentModes,
        },
      });
    } catch (err) {
      await t.rollback();
      console.log("getting error...");
      console.error(err);
      res.status(400).json({
        error: err,
      });
    }
  };
  
  const getAllPaymentModes = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
      const paymentModes = await findAllPaymentModes(t);
      await t.commit();
      res.status(200).json({
        status: "success",
        length:paymentModes.length,
        data: {
        paymentModes,
        },
      });
    } catch (err) {
      await t.rollback();
      console.log("getting error...");
      console.error(err);
      res.status(400).json({
        error: err,
      });
    }
  };
  const deletedPaymentModes = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
      const id= req.params.id;
      const PaymentModes=await PaymentModes.findByPk(id);
      if(!PaymentModes){
        res.status(404).json({
            message: "Does Not exist a Customer with id = " + id,
            error: "404",
        });
    } else {
        await deletePaymentModes(id,t);
        res.status(200).json({
            message: "Delete Successfully a Customer with id = " + id,
           
        });
    }
      await t.commit();
    } catch (err) {
      await t.rollback();
      console.log("getting error...");
      console.error(err);
      res.status(400).json({
        error: err,
      });
    }
  };
  
  const updatedPaymentModes = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
      // const employee = await findAllEmployee(t);
      const id= req.params.id;
      const paymentModes=await PaymentModes.findByPk(id);
      if(!paymentModes){
        res.status(404).json({
          message: "Not Found for updating a PaymentModes with id = " + id,
          customer: "",
          error: "404"
      });
      }else{
    const data=req.body;
        let result = await updatePaymentModes(id,data,t)
        if(!result) {
          res.status(500).json({
              message: "Error -> Can not update a PaymentModes with id = " + req.params.id,
              error: "Can NOT Updated",
          });
      }
      res.status(200).json({
        message: "Update successfully a PaymentModes with id = " + id,
        employee: result,
    });
      }
      await t.commit();
    } catch (err) {
      await t.rollback();
      console.log("getting error...");
      console.error(err);
      res.status(400).json({
        error: err,
      });
    }
  };

  export {createPaymentModes,getAllPaymentModes,deletedPaymentModes,updatedPaymentModes}