import express from "express";

import { addBank, getAllBank} from "./bank.controller.mjs";

const bankRouter = express.Router();

bankRouter.get("/", getAllBank);
bankRouter.post("/",addBank);

export default bankRouter;