import { sequelize } from "../../services/postgres.mjs";
import Branch from "../../models/branch/branch.postgres.mjs"
import { addBranch, findAllBranch,deleteBranch,updateBranch,branchById,branchByOrganisationId,addBranchByOrganisationId } from "../../models/branch/branch.model.mjs";

const createBranch = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const branch = await addBranch(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        branch,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const createBranchByOrganisationId = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id=req.params.id
    const bodyData = req.body;
    console.log(id)
    const branch = await addBranchByOrganisationId(id,bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        branch,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};


const getAllBranch = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const branches = await findAllBranch(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: branches.length,
      data: {
        branches,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const getBranchById=async(req,res,next)=>{
  const t = await sequelize.transaction();
  try {
    const id=req.params.id;
    const branch=await branchById(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      length: branch.length,
      data: {
        branch,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}
const getBranchByOrganisationId=async(req,res,next)=>{
  const t = await sequelize.transaction();
  try {
    const id=req.params.id;
    const branch=await branchByOrganisationId(id,t)
    await t.commit();
    res.status(200).json({
      status: "success",
      length: branch.length,
      data: {
        branch,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}
const deletedBranch = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const branch=await Branch.findByPk(id);
    if(!branch){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteBranch(id,t);
      res.status(204).json({
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedBranch = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const branch=await Branch.findByPk(id);
    if(!branch){
      res.status(404).json({
        message: "Not Found for updating a Branch with id = " + id,
        customer: "",
        error: "404"
    });
    }else{
  const data=req.body;
      let result = await updateBranch(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Branch with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a branch with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};


export { createBranch, getAllBranch,deletedBranch,updatedBranch,getBranchById,getBranchByOrganisationId,createBranchByOrganisationId };
