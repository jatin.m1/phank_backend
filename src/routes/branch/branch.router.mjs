import express from "express";

import { createBranch,
     getAllBranch,
     deletedBranch,
     updatedBranch, 
     getBranchById, 
     getBranchByOrganisationId,
     createBranchByOrganisationId} from "./branch.controller.mjs";

const branchRouter = express.Router();

branchRouter.get("/", getAllBranch);
branchRouter.post("/",createBranch);
branchRouter.delete("/:id",deletedBranch)
branchRouter.put("/:id",updatedBranch)
branchRouter.get("/:id",getBranchById)
branchRouter.get("/organisation/:id",getBranchByOrganisationId)
branchRouter.post("/organisation/:id",createBranchByOrganisationId)

export default branchRouter;