import express from "express";

import { createStatus, getAllStatus ,deletedStatus,updatedStatus, } from "./status.controller.mjs";

const statusRouter = express.Router();

statusRouter.get("/", getAllStatus);
statusRouter.post("/", createStatus);
statusRouter.delete("/:id", deletedStatus);
statusRouter.put("/:id",updatedStatus)

export default statusRouter;
