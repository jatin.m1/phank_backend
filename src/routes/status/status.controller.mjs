import { sequelize } from "../../services/postgres.mjs";
import Status from "../../models/status/status.postgres.mjs";
import { addStatus ,findAllStatus,deleteStatus,updateStatus} from "../../models/status/status.model.mjs";

const createStatus = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const status = await addStatus(bodyData, t);
    await t.commit();
    res.status(201).json({
      status: "success",
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllStatus = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const status = await findAllStatus(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: status.length,
      data: {
        status,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
const deletedStatus = async (req, res, next) => {

  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const status=await Status.findByPk(id);
    if(!status){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteStatus(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedStatus = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
   
    const status=await Status.findByPk(id);
    if(!status){
      res.status(404).json({
        message: "Not Found for updating a Status with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

      const data=req.body

      let result = await updateStatus(id,data.name,data.color,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Status with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Status with id = " + id,
      
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
}


export { createStatus, getAllStatus ,deletedStatus,updatedStatus};
