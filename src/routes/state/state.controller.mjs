import { sequelize } from "../../services/postgres.mjs";
import State from "../../models/state/state.postgres.mjs"
import { addState, findAllState,deleteState,updateState,stateByCountrId} from "../../models/state/state.model.mjs";

const createState = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const state = await addState(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        state,
      }, 
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllState = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const states = await findAllState(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: states.length,
      data: {
        states,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getStateByCountryId = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id=req.params.id;
    const states = await stateByCountrId(id,t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: states.length,
      data: {
        states,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const deletedState = async (req, res, next) => {

  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const state=await State.findByPk(id);
    if(!state){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteState(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
          State: State,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedState = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
   
    const state=await State.findByPk(id);
    console.log("ram")
    console.log(state)
    if(!state){
      res.status(404).json({
        message: "Not Found for updating a State with id = " + id,
        customer: "",
        error: "404"
    });
    }else{

      const data=req.body

      let result = await updateState(id,data.name,data.color,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a State with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a State with id = " + id,
     
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};


export { createState, getAllState,deletedState,updatedState,getStateByCountryId};
