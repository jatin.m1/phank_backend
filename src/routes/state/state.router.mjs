import express from "express";

import { createState, getAllState,deletedState,updatedState, getStateByCountryId} from "./state.controller.mjs";

const stateRouter = express.Router();

stateRouter.get("/", getAllState);
stateRouter.post("/",createState);
stateRouter.delete("/:id",deletedState)
stateRouter.put("/:id",updatedState)
stateRouter.get("/country/:id",getStateByCountryId)

 
export default stateRouter;