import express from "express";

import { createOrganisation, getAllOrganisation,deletedOrganisation,updatedOrganisation} from "./organisation.controller.mjs";

const OrganisationRouter = express.Router();

OrganisationRouter.get("/", getAllOrganisation);
OrganisationRouter.post("/",createOrganisation);
OrganisationRouter.delete("/:id",deletedOrganisation)
OrganisationRouter.put("/:id",updatedOrganisation)
// OrganisationRouter.get("/:id",getOrganisationById)

export default OrganisationRouter;