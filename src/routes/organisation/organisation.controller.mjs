import { sequelize } from "../../services/postgres.mjs";
import Organisation from "../../models/organisation/organisation.postgres.mjs"
import { addOrganisation, findAllOrganisation,deleteOrganisation,updateOrganisation} from "../../models/organisation/organisation.model.mjs";

const createOrganisation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const bodyData = req.body;
    const organisation = await addOrganisation(bodyData, t);
    await t.commit();

    res.status(201).json({
      status: "success",
      data: {
        organisation,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const getAllOrganisation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const organisations = await findAllOrganisation(t);
    await t.commit();
    res.status(200).json({
      status: "success",
      length: organisations.length,
      data: {
        organisations,
      },
    });
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};
// const getOrganisationById=async(req,res,next)=>{
//   const t = await sequelize.transaction();
//   try {
//     const id=req.params.body;
//     const organisation=await OrganisationById(id,t)
//     await t.commit();
//     res.status(200).json({
//       status: "success",
//       length: organisation.length,
//       data: {
//         organisation,
//       },
//     });
//   } catch (err) {
//     await t.rollback();
//     console.log("getting error...");
//     console.error(err);
//     res.status(400).json({
//       error: err,
//     });
//   }
// }

const deletedOrganisation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const id= req.params.id;
    const organisation=await Organisation.findByPk(id);
    if(!organisation){
      res.status(404).json({
          message: "Does Not exist a Customer with id = " + id,
          error: "404",
      });
  } else {
      await deleteOrganisation(id,t);
      res.status(200).json({
          message: "Delete Successfully a Customer with id = " + id,
      });
  }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};

const updatedOrganisation = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    // const employee = await findAllEmployee(t);
    const id= req.params.id;
    const organisation=await Organisation.findByPk(id);
    if(!organisation){
      res.status(404).json({
        message: "Not Found for updating a Organisation with id = " + id,
        customer: "",
        error: "404"
    });
    }else{
  const data=req.body;
      let result = await updateOrganisation(id,data,t)
      if(!result) {
        res.status(500).json({
            message: "Error -> Can not update a Organisation with id = " + req.params.id,
            error: "Can NOT Updated",
        });
    }
    res.status(200).json({
      message: "Update successfully a Organisation with id = " + id,
      employee: result,
  });
    }
    await t.commit();
  } catch (err) {
    await t.rollback();
    console.log("getting error...");
    console.error(err);
    res.status(400).json({
      error: err,
    });
  }
};


export { createOrganisation, getAllOrganisation,deletedOrganisation,updatedOrganisation};
