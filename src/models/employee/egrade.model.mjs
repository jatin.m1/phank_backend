import Egrade from './egrade.postgres.mjs'



const addEgrade = async (data, transaction) => {
  return await Egrade.create(data, { transaction });
};

const findAllEgrade= async (transaction) => {
  return await Egrade.findAll({ transaction });
};

const deleteEgrade= async (id,transaction) => {
  return await Egrade.destroy({
    where:{id:id}, transaction });
};

const updateEgrade = async (id,data,transaction) => {
  return await Egrade.update({
    name:data.name,  
  },{
    where:{
      id:id,
    },transaction
  });
};


export { addEgrade, findAllEgrade,deleteEgrade,updateEgrade };  