import Gender from "./gender.postgres.mjs"



const addGender = async (data, transaction) => {
  return await Gender.create(data, { transaction });
}; 

const findAllGender = async (transaction) => {
  return await Gender.findAll({ transaction });
};

const deleteGender = async (id,transaction) => {
  return await Gender.destroy({
    where:{id:id}, transaction });
};

const updateGender = async (id,data,transaction) => {
  return await Gender.update({
    name:data.name,
    color:data.color,  
  },{
    where:{
      id:id,
    },transaction
  });
};

export { addGender, findAllGender ,deleteGender,updateGender};      
