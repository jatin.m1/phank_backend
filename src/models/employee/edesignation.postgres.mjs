import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Edesignation = sequelize.define("Edesignation", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter test name",
      },
    },
  },
  status:{
    type: DataTypes.UUID,
   
  },
  organisationId:{
    type: DataTypes.UUID,
    
  }

});

export default Edesignation;
