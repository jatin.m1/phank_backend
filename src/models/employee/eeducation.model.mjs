import Eeducation from './eeducation.postgres.mjs'



const addEeducation = async (data, transaction) => {
  return await Eeducation.create(data, { transaction });
};

const findEeducationByEmployeeId= async (id,transaction) => {
    console.log(id)
  return await Eeducation.findOne({where:{
      employeeId:id
  }},{ transaction });
};

const deleteEeducation= async (id,transaction) => {
  return await Eeducation.destroy({
    where:{employeeId:id}, transaction });
};

const updateEeducation = async (id,data,transaction) => {
  return await Eeducation.update({
    school:data.school,  
    yearOfPassing:data.yearOfPassing,
    certificate:data.certificate
  },{
    where:{
        employeeId:id,
    },transaction
  });
};


export { addEeducation, findEeducationByEmployeeId,deleteEeducation,updateEeducation };  