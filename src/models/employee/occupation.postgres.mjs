import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Occupation = sequelize.define("Occupation", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter test name",
      },
    },
  },


});


export default Occupation;