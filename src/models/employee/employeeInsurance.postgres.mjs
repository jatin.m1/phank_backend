import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const EmployeInsurance = sequelize.define("EmployeInsurance", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  InsuranceId:{
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4, 
  }
 
});


export default EmployeInsurance;