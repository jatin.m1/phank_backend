import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Efamily = sequelize.define("Efamily", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter test name",
      },
    },
  },
  relationship:{
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
  },
  dob:Date,
  genderId:{
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
  },
  occupation :{
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
  },
  isEmergencyContact:{
      type:Boolean,
      defaultValue:false
  }
});


export default Efamily;
