import Relationship from "./relationship.postgres.mjs"



const addRelationship = async (data, transaction) => {
  return await Relationship.create(data, { transaction });
};

const findAllRelationship = async (transaction) => {
  return await Relationship.findAll({ transaction });
};

export { addRelationship, findAllRelationship };      
