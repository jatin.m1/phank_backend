import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Estatus = sequelize.define("Estatus", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter test name",
      },
    },
  },
});

export default Estatus;
