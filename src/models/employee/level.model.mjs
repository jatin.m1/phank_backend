import Level from "./level.postgres.mjs"



const addLevel = async (data, transaction) => {
  return await Level.create(data, { transaction });
}; 

const findAllLevel = async (transaction) => {
  return await Level.findAll({ transaction });
};

const deleteLevel = async (id,transaction) => {
  return await Level.destroy({
    where:{id:id}, transaction });
};

const updateLevel = async (id,data,transaction) => {
  return await Level.update({
 
  },{
    where:{
      id:id,
    },transaction
  });
};

export { addLevel, findAllLevel ,deleteLevel,updateLevel};      
