import Edesignation from './edesignation.postgres.mjs'
import Status from '../status/status.postgres.mjs';



const addEdesignation = async (data, transaction) => {
  return await Edesignation.create(data, { transaction });
};

const findAllEdesignation= async (transaction) => {
  return await Edesignation.findAll({include:[{model:Status}], transaction });
  //add atrributes
};

const deleteEdesignation= async (id,transaction) => {
  return await Edesignation.destroy({
    where:{id:id}, transaction });
};

const updateEdesignation = async (id,data,transaction) => {
  return await Edesignation.update({
    name:data.name,  
  },{
    where:{
      id:id,
    },transaction
  });
};


export { addEdesignation, findAllEdesignation,deleteEdesignation,updateEdesignation };  