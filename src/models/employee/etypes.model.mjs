import Etype from "./etypes.postgres.mjs"
import Status from "../status/status.postgres.mjs"



const addEtype = async (data, transaction) => {
  return await Etype.create(data, { transaction });
}; 

const findAllEtype = async (transaction) => {
  return await Etype.findAll({ transaction });
};

const deleteEtype = async (id,transaction) => {
  return await Etype.destroy({
    where:{id:id}, transaction });
};

const updateEtype = async (id,data,transaction) => {
  return await Etype.update({
    name:data.name,
    color:data.color,  
  },{
    where:{
      id:id,
    },transaction
  });
};

export { addEtype, findAllEtype ,deleteEtype,updateEtype};      
