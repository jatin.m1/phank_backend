import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const EmployeVehicleDetail = sequelize.define("EmployeVehicleDetail", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  vehicalId:{
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
  },
  dlNUmber:{
    type: DataTypes.STRING(20),
  },
  dlImage:{
    type: DataTypes.STRING(20),
  }
 


});


export default EmployeVehicleDetail;