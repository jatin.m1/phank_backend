import Manager from "./manager.postgres.mjs"



const addManager = async (data, transaction) => {
  return await Manager.create(data, { transaction });
}; 

const findAllManager = async (transaction) => {
  return await Manager.findAll({ transaction });
};

const deleteManager = async (id,transaction) => {
  return await Manager.destroy({
    where:{id:id}, transaction });
};

const updateManager = async (id,data,transaction) => {
  return await Manager.update({
    name:data.name,
    color:data.color,  
  },{
    where:{
      id:id,
    },transaction
  });
};

export { addManager, findAllManager ,deleteManager,updateManager};      
