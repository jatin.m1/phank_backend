import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Eworkhistory = sequelize.define("Eworkhistory", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  companyName: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter company name",
      },
    },
  },
  designation:{
    type: DataTypes.STRING(20)
  },
  salary:{
    type: DataTypes.STRING(20)
  },
  from :DataTypes.DATE,
  to :DataTypes.DATE,
  location:{
    type: DataTypes.STRING(20)
  },
  document:{
    type: DataTypes.STRING(20)
  },
  employeeId:{
    type: DataTypes.UUID,
  }

});


export default Eworkhistory;
