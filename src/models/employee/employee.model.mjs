import Employee from "./employe.postgres.mjs"



const addEmployee = async (data, transaction) => {
  return await Employee.create({name:data.name,employeeId:data.employeeId,
  bio:data.bio,
genderId:data.genderId,
dp:data.dp,
dob:data.dob,
doj:data.doj,
dor:data.dor,
lwd:data.lwd,
etypeId:data.etypeId,
status:data.status,
hasHInsurance:data.hasHInsurance,
preferedpaymentMode:data.preferedpaymentMode,
isManager:data.isManager,
managerId:data.managerId,
isVehicalAssigned:data.isVehicalAssigned,
departmentId:data.departmentId,
branchId:data.branchId},{ transaction });
}; 

const findAllEmployee = async (transaction) => {
  return await Employee.findAll({ transaction });
};
const deleteEmployee = async (id,transaction) => {
  return await Employee.destroy({
    where:{
    id:id
  } ,transaction });
};
const updateEmployee = async (id,data,transaction) => {
  return await Employee.update(
    {id:data.id,
    name:data.name,
    bio:data.bio},
    {where:{id:id},transaction });
}; 
const employeeById=async(id,trnsaction)=>{
  return await Employee.findByPk(id,{transaction});
}
const employeeByDepartmentId=async(id,transaction)=>{
  return await Employee.findAll({where:{
    departmentId:id
  }},{transaction});
}
const employeeByBranchId=async(id,transaction)=>{
  return await Employee.findAll({where:{
    branchId:id
  }},{transaction});
}

export { addEmployee, findAllEmployee ,deleteEmployee,updateEmployee,employeeById,employeeByDepartmentId,employeeByBranchId};      
