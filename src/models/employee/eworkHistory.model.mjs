import Eworkhistory from "./eworkHistory.postgres.mjs"



const addEworkhistory = async (data, transaction) => {
  return await Eworkhistory.create(data, { transaction });
}; 

const findAllEworkhistory = async (transaction) => {
  return await Eworkhistory.findAll({ transaction });
};
const findEworkhistoryByEmployeeId = async (id,transaction) => {
    return await Eworkhistory.findOne({where:{
        employeeId:id
    }},{ transaction });
  };
  

const deleteEworkhistory = async (id,transaction) => {
  return await Eworkhistory.destroy({
    where:{employeeId:id}, transaction });
};

const updateEworkhistory = async (id,data,transaction) => {
  return await Eworkhistory.update({
    companyName:data.companyName,
    designation:data.designation, 
    salary:data.salary,
    from:data.from,
    to:data.to,
    location:data.location,
    document:data.document,
  },{
    where:{
      employeeId:id,
    },transaction
  });
};

export { addEworkhistory, findAllEworkhistory ,deleteEworkhistory,updateEworkhistory,findEworkhistoryByEmployeeId};      
