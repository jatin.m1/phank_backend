import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const EmployeMetaData = sequelize.define("EmployeMetaData", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  edepartmentId: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    allowNull: false,
  },
  egrade:{
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    allowNull: false,
  },
  edesignationId:{
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    allowNull: false,
  },
  brancId:{
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4, 
  }


});


export default EmployeMetaData;