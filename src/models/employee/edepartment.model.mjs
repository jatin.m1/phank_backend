import Edepartment from "./edepartment.postgres.mjs";
import Status from "../status/status.postgres.mjs"
 

const addEdepartment = async (data, transaction) => {
  return await Edepartment.create(data, { transaction });
};

const findAllEdepartment = async (transaction) => {
  return await Edepartment.findAll({include:{model:Status,attributes:["id","name","color"]}, transaction });
}; 

const findEdepartmentById = async (id,transaction) => {
  return await Edepartment.findAll({  where: {
    id: id
  }
},{ transaction });
}; 
const deleteEdepartment = async (id,transaction) => {
  return await Edepartment.destroy({
    where:{id:id}, transaction });
};

const updateEdepartment = async (id,data,transaction) => {
  return await Edepartment.update({
    name:data.name,
    color:data.color,  
  },{
    where:{
      id:id,
    },transaction
  });
};

 

export { addEdepartment, findAllEdepartment,deleteEdepartment,updateEdepartment ,findEdepartmentById};      