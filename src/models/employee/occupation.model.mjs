import Occupation from "./occupation.postgres.mjs"



const addOccupation = async (data, transaction) => {
  return await Occupation.create(data, { transaction });
};

const findAllOccupation = async (transaction) => {
  return await Occupation.findAll({ transaction });
};

export { addOccupation, findAllOccupation };      
