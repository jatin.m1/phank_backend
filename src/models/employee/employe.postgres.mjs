import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Employee = sequelize.define("Employee", {
  id: {
    type: DataTypes.UUID, 
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  employeeId: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
  },
  name:{
    type: DataTypes.STRING(20),
    allowNull: false, 
  },
  bio:{
    type: DataTypes.STRING(20),
  },
  genderId:{
    type: DataTypes.UUID,

  },
  dp:{
    type: DataTypes.STRING(20),
  },
  dob:DataTypes.DATE,
  doj:DataTypes.DATE,
  dor:DataTypes.DATE,
  lwd:DataTypes.DATE,
  etypeId:{
    type: DataTypes.UUID,
  }, 
  status:{
    type: DataTypes.UUID,
  },
  hasHInsurance:{
      type:DataTypes.BOOLEAN,
      defaultValue:false
  },
  preferedpaymentMode:{
    type: DataTypes.UUID,
  },
  isManager:{
    type:DataTypes.BOOLEAN,
    defaultValue:false
  },
  managerId:{
    type: DataTypes.UUID,
  },
  isVehicalAssigned:{
    type:DataTypes.BOOLEAN,
    defaultValue:false
  },
departmentId:{
  type:DataTypes.UUID
},
branchId:{
  type:DataTypes.UUID
}

});


export default Employee;