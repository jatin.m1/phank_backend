import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Emetadata = sequelize.define("Emetadata", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter  name",
      },
    },
  },
edepartmentId:{
    type:DataTypes.UUID,
    allowNull: false,
    validate: {
        notNull: {
          msg: "Please enter edepartmentId",
        },
      }
},
egrade:{
    type:DataTypes.UUID,
    allowNull: false,
    validate: {
        notNull: {
          msg: "Please enter  egrade",
        },
      }
},
edesignationId:{
    type:DataTypes.UUID ,
    allowNull: false,
    validate: {
        notNull: {
          msg: "Please enter  edesignationID",
        },
      }
},
branchId:{
    type:DataTypes.UUID
},
employeeId:{
    type:DataTypes.UUID ,
    unique:true,
    allowNull: false,
    validate: {
        notNull: {
          msg: "Please enter  employeeID",
        },
      }
}
});

export default Emetadata;
