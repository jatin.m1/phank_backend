import Qualification from "./qualification.postgres.mjs"



const addQualification = async (data, transaction) => {
  return await Qualification.create(data, { transaction });
}; 

const findAllQualification = async (transaction) => {
  return await Qualification.findAll({ transaction });
};

const deleteQualification = async (id,transaction) => {
  return await Qualification.destroy({
    where:{id:id}, transaction });
};

const updateQualification = async (id,data,transaction) => {
  return await Qualification.update({
 
  },{
    where:{
      id:id,
    },transaction
  });
};

export { addQualification, findAllQualification ,deleteQualification,updateQualification};      
