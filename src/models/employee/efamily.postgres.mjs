import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Efamily = sequelize.define("Efamily", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter test name",
      },
    },
  },
  relationship:{
    type: DataTypes.UUID
  },
  dob:{
      type:DataTypes.DATE
  },
  genderId:{ 
      type:DataTypes.UUID
  },
  occupationId:{
    type:DataTypes.UUID 
  },
  isEmergencyContact:{
      type:DataTypes.BOOLEAN,
      defaultValue:false
  },
  employeeId:{
    type:DataTypes.UUID 
  }

});

export default Efamily;
