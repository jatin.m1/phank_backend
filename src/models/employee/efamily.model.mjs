import Efamily from './efamily.postgres.mjs'



const addEfamily = async (data, transaction) => {
  return await Efamily.create(data, { transaction });
};

const findEfamilyByEmployeeId= async (id,name,emg,transaction) => {
    console.log(id)
    var family
    if(!name){
      if(!emg){
        family=await await Efamily.findAll({where:{
          employeeId:id,
      }},{ transaction })
      }else{
        family=await await Efamily.findOne({where:{
          employeeId:id,
          isEmergencyContact:emg
      }},{ transaction })
      }

    }else{
      if(!emg){
        family=await await Efamily.findAll({where:{
          employeeId:id,
          name:name
      }},{ transaction })
      }else{
        family=await await Efamily.findOne({where:{
          employeeId:id,
          name:name,
          isEmergencyContact:emg
      }},{ transaction })
      }

    }
  return  family
};

const deleteEfamily= async (id,name,transaction) => {
 
  var family
  if(!name){
    family=await Efamily.destroy({where:{
      employeeId:id,
  }},transaction)
  }else{
    family=await Efamily.destroy({where:{
      employeeId:id,
      name:name
  }},transaction)
  }
  return family
};

const updateEfamily = async (id,data,name,transaction) => {

  var family
  if(!name){
    family=await Efamily.update({
      name:data.name,  
      dob:data.dob,
    },{where:{
      employeeId:id,
  }},transaction)
  }else{
    family=await Efamily.update({
      name:data.name,  
      dob:data.dob,
    },{where:{
      employeeId:id,
      name:name
  }},transaction)
  }
  return family;
};


export { addEfamily, findEfamilyByEmployeeId,deleteEfamily,updateEfamily };  