import EmetaData from './emetaData.postgres.mjs'



const addEmetaData = async (data, transaction) => {
  return await EmetaData.create(data, { transaction });
};

const findAllEmetaData= async (id,transaction) => {
  return await EmetaData.findAll({where:{
      employeeId:id
  }},{ transaction });
};

const deleteEmetaData= async (id,transaction) => {
  return await EmetaData.destroy({
    where:{employeeId:id}, transaction });
};

const updateEmetaData = async (id,data,transaction) => {
  console.log(data)
  return await EmetaData.update({
    name:data.name
  },{
    where:{
      employeeId:id,
    },transaction
  });
};


export { addEmetaData, findAllEmetaData,deleteEmetaData,updateEmetaData };  