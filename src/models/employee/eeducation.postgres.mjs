import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Eeducation = sequelize.define("Eeducation", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  school: {
    type: DataTypes.STRING(20),
  },
  qualificationId:{
    type: DataTypes.UUID,
  },
  levelId:{
    type: DataTypes.UUID,

  },
  yearOfPassing:{
    type:DataTypes.DATE
  },
  certificate:{
    type: DataTypes.STRING(20),
  },
  employeeId:{
    type: DataTypes.UUID,
    unique:true

  }
  

});


export default Eeducation;
