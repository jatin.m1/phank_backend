import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Edepartment = sequelize.define("Edepartment", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true, 
    allowNull: false, 
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter test name",
      },
    },
  },
  color:{
    type: DataTypes.STRING(20)
  },
  status:{
    type: DataTypes.UUID,
  },
  organisationId:{
    type: DataTypes.UUID,
  },


});

export default Edepartment;
