import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Etype = sequelize.define("Etype", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter test name",
      },
    },
  },
  color:{
    type: DataTypes.STRING(20)
  },
  status:{
    type: DataTypes.UUID,
    allowNull: false,
  }
});

export default Etype;
