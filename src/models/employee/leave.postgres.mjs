import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Leave = sequelize.define("Leave", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  }
  
});


export default Leave;
