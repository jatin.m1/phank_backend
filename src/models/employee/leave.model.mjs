import Leave from "./leave.postgres.mjs"



const addLeave = async (data, transaction) => {
  return await Leave.create(data, { transaction });
}; 

const findAllLeave = async (transaction) => {
  return await Leave.findAll({ transaction });
};

const deleteLeave = async (id,transaction) => {
  return await Leave.destroy({
    where:{id:id}, transaction });
};

const updateLeave = async (id,data,transaction) => {
  return await Leave.update({
 
  },{
    where:{
      id:id,
    },transaction
  });
};

export { addLeave, findAllLeave ,deleteLeave,updateLeave};      
