import Occupation from './occupation.postgres.mjs'

const addOccupation= async (data, transaction) => {
  return  await Occupation.create(data,transaction);
};

const findAllOccupation = async (transaction) => {
  return await Occupation.findAll({ transaction });
};

const deleteOccupation = async (id,transaction) => {
  return await Occupation.destroy({
    where:{id:id}, transaction });
};

const updateOccupation = async (id,name,address,transaction) => {
  return await Occupation.update({
    name:name, 
    address:address
  },{
    where:{
      id:id,
    },transaction
  });
};
export { addOccupation, findAllOccupation ,deleteOccupation,updateOccupation};      
