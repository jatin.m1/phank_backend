import State from "./state.postgres.mjs";



const addState= async (data, transaction) => {
  console.log(data.color)
  console.log(data.name)
    return  await State.create({name:data.name,color:data.color,countryId:data.countryId},transaction);
  };
  
  const findAllState = async (transaction) => {
    return await State.findAll({ transaction });
  };
  
  const deleteState = async (id,transaction) => {
    console.log(id)
    return await State.destroy({
      where:{id:id}, transaction });
  };
  
  const updateState = async (id,name,color,transaction) => {
    return await State.update({
      name:name, 
      color:color
    },{
      where:{
        id:id,
      },transaction
    });
  };

  const stateByCountrId=async(id,transaction)=>{
    return await State.findAll({where:{
      countryId:id
    }},{ transaction });
  }
  export { addState, findAllState ,deleteState,updateState,stateByCountrId};       