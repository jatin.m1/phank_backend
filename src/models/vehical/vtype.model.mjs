import Vtype from "./vtype.postgres.mjs";


const addVtype = async (data, transaction) => {
  return await Vtype.create(data, { transaction });
};

const findAllVtype = async (transaction) => {
  return await Vtype.findAll({ transaction });
};


const deleteVtype = async (id,transaction) => {
  return await Vtype.destroy({
    where:{id:id}, transaction });
};

const updateVtype = async (id,data,transaction) => {
  return await Vtype.update({
    name:data.name, 
  },{
    where:{
      id:id,
    },transaction
  });
};
const vtypeById=async(id,trnsaction)=>{
  return await Vtype.findByPk(id,{transaction});
}




export { addVtype , findAllVtype ,deleteVtype,updateVtype,vtypeById};