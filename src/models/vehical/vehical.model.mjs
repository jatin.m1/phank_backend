import Vehical from "./vehical.postgres.mjs";


const addVehical = async (data, transaction) => {
  return await Vehical.create(data, { transaction });
};

const findAllVehical = async (transaction) => {
  return await Vehical.findAll({ transaction });
};

const deleteVehical = async (id,transaction) => {
  return await Vehical.destroy({
    where:{id:id}, transaction });
};

const updateVehical = async (id,data,transaction) => {
  return await Vehical.update({
    name:data.name, 
  },{
    where:{
      id:id,
    },transaction
  });
};
const vehicalById=async(id,transaction)=>{
  return await Vehical.findByPk(id,{transaction});
}
const vehicalByOrganisationId=async(id,transaction)=>{
  return await Vehical.findAll({
    where:{
      organisationId:id
    }
  },{transaction});
}
const vehicalByBranchId=async(id,transaction)=>{
  return await Vehical.findAll({
    where:{
      branchId:id
    }
  },{transaction});
}

const vehicalByEmployeeId=async(id,transaction)=>{
  return await Vehical.findAll({
    where:{
      employeeId:id
    }
  },{transaction});
}
const updateVehicalByEmployeeId = async (id,data,transaction) => {
  return await Vehical.update({
    name:data.name, 
  },{
    where:{
      employeeId:id,
    },transaction
  });
};


export { addVehical ,
   findAllVehical ,
   deleteVehical,
   updateVehical,
   vehicalById,
   vehicalByOrganisationId,
   vehicalByBranchId,
   vehicalByEmployeeId,
   updateVehicalByEmployeeId };      