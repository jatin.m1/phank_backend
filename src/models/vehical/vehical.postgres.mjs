import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Vehical = sequelize.define("Vehical", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter vehical name",
      }
    }
  },
  rcNumber:{
  type: DataTypes.STRING(20),
  allowNull: false,
  unique:true,
  validate: {
    notNull: {
      msg: "Please enter rcNumber",
    },
  },
},
vmake:{
  type: DataTypes.UUID,
  defaultValue: DataTypes.UUIDV4,
},
vtype:{
  type: DataTypes.UUID,
},
chasisNumber:{
  type: DataTypes.STRING(20),
},
rcImage:{
  type: DataTypes.STRING(20),
},

branchId:{
  type: DataTypes.UUID,
  
},
pucImage:{
  type: DataTypes.STRING(20),
},
organisationId:{
  type: DataTypes.UUID,
  
},
pucEndDate:DataTypes.DATE,

employeeId:{
  type: DataTypes.UUID,
}


});

export default Vehical;
















