import Vmake from "./vmake.postgres.mjs";



const addVmake= async (data, transaction) => {
    return  await Vmake.create({name:data.name,color:data.color},transaction);
  };
  
  const findAllVmake = async (transaction) => {
    return await Vmake.findAll({ transaction });
  };
  
  const deleteVmake = async (id,transaction) => {
    return await Vmake.destroy({
      where:{id:id}, transaction });
  };
  
  const updateVmake = async (id,name,color,transaction) => {
    return await Vmake.update({
      name:name, 
      color:color
    },{
      where:{
        id:id,
      },transaction
    });
  };



export { addVmake , findAllVmake ,deleteVmake,updateVmake};