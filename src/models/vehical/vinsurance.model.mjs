import Vinsurance from "./vinsurance.postgres.mjs";


const addVinsurance= async (data, transaction) => {
  return await Vinsurance.create(data, { transaction });
};

const findVinsuranceByVehicalId= async (id,transaction) => {
  return await Vinsurance.findOne({where:{vehicalId:id}},{ transaction });
};

const deleteVinsurance= async (id,transaction) => {
  return await Vinsurance.destroy({
    where:{id:id}, transaction });
};

const updateVinsurance= async (id,data,transaction) => {
  return await Vinsurance.update({
    name:data.name, 
    insuranceImage:data.insuranceImage,
    insuranceStartDate:data.insuranceStartDate,
    insuranceEndDate:data.insuranceEndDate,
    isCurrent:data. isCurrent
  },{
    where:{
      vehicalId:id,
    },transaction
  });
};
const VinsuranceById=async(id,transaction)=>{
  return await Vinsurance.findByPk(id,{transaction});
}



export { addVinsurance,
    findVinsuranceByVehicalId,
   deleteVinsurance,
   updateVinsurance,

};      