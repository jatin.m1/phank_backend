import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const EbankDetails = sequelize.define("EbankDetails", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  accountNumber:{
    type: DataTypes.STRING(20),
    allowNull: false,
  },
  bankId:{
    type: DataTypes.UUID,
  },
  branchAddress:{
    type: DataTypes.STRING(20),
  },
  upiId:{
    type: DataTypes.STRING(20),
  },
  isDefault:{
    type: DataTypes.BOOLEAN,
    defaultValue:false
  },
  employeeId:{
    type: DataTypes.UUID,
    allowNull: false,
    validate: {
      notNull: {
        msg: "Please enter  employeeID",
      },
    }

    
  }

  
});


export default EbankDetails;
