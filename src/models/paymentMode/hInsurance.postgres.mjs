import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Hinsurance = sequelize.define("Hinsurance", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  nanme:{
    type: DataTypes.STRING(20),
    allowNull: false,
    validate: {
      notNull: {
        msg: "Please enter name",
      },
    }
  },
  insuranceNumber:{
    type: DataTypes.STRING(20),
    allowNull: false,
    validate: {
      notNull: {
        msg: "Please enter insurance number",
      },
    }
  }
  
});


export default Hinsurance;
