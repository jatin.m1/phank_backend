import Bank from './bank.postgres.mjs'



const createBank = async (data, transaction) => {
  return await Bank.create(data, { transaction });
};

const findAllBank= async (transaction) => {
  return await Bank.findAll({ transaction });
};



export { createBank, findAllBank };  