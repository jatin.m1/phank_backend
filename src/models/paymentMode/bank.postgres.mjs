import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Bank = sequelize.define("Bank", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter test name",
      },
    },
  },
  ifscCode:{
    type: DataTypes.STRING(20),
    allowNull: false,
    validate: {
      notNull: {
        msg: "Please enter code",
      },
    },
  }
  
});


export default Bank;
