import EbankDetails from './ebankDetails.postgres.mjs'

const addEbankDetails= async (data, transaction) => {
  return  await EbankDetails.create(data,transaction);
};

const findAllEbankDetails = async (id,dval,transaction) => {

  var res;
  if(!dval){
    res= await EbankDetails.findAll({where:{employeeId:id}},{ transaction });
  }else{
    res = await EbankDetails.findOne({where:{employeeId:id,isDefault:dval}},{ transaction });
  }
  return res
};

const deleteEbankDetails = async (id,transaction) => {
  return await EbankDetails.destroy({
    where:{employeeId:id}, transaction });
};

const updateEbankDetails = async (id,data,transaction) => {
  return await EbankDetails.update({
    accountNumber:data.accountNumber,
    branchAddress:data.branchAddress,
    upiId:data.upiId,
  },{
    where:{
      employeeId:id,
    },transaction
  });
};
export { addEbankDetails, findAllEbankDetails ,deleteEbankDetails,updateEbankDetails};      
