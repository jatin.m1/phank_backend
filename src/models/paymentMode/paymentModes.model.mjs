import PaymentModes from './paymentModes.postgres.mjs'



const addPaymentModes = async (data, transaction) => {
  return await PaymentModes.create(data, { transaction });
};

const findAllPaymentModes= async (transaction) => {
  return await PaymentModes.findAll({ transaction });
};

const deletePaymentModes = async (id,transaction) => {
  return await PaymentModes.destroy({
    where:{id:id}, transaction });
};

const updatePaymentModes = async (id,data,transaction) => {
  return await PaymentModes.update({
    name:data.name,
    color:data.color,  
  },{
    where:{
      id:id,
    },transaction
  });
};





export { addPaymentModes, findAllPaymentModes,deletePaymentModes,updatePaymentModes };  