import Status from "./status.postgres.mjs";



const addStatus= async (data, transaction) => {
    return  await Status.create({name:data.name,color:data.color},transaction);
  };
  
  const findAllStatus = async (transaction) => {
    return await Status.findAll({ transaction });
  };
  
  const deleteStatus = async (id,transaction) => {
    return await Status.destroy({
      where:{id:id}, transaction });
  };
  
  const updateStatus = async (id,name,color,transaction) => {
    return await Status.update({
      name:name, 
      color:color
    },{
      where:{
        id:id,
      },transaction
    });
  };
export { addStatus, findAllStatus ,deleteStatus,updateStatus};      
