import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Branch = sequelize.define("Branch", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    unique:true,
    validate: {
      notNull: {
        msg: "Please enter test name",
      },
    },
  },
  address:{
    type: DataTypes.STRING(20)
  },
  statusId:{
    type: DataTypes.UUID,
  },
  organisationId:{
    type: DataTypes.UUID,
  },
  stateId:{
    type: DataTypes.UUID,
  },

});

export default Branch;
