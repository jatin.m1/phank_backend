import Branch from "./branch.postgres.mjs";



const addBranch = async (data, transaction) => {
  return await Branch.create({
    name:data.name,
    address:data.address,
    
  }, { transaction });
}; 
const addBranchByOrganisationId = async (id,data, transaction) => {
  return await Branch.create({
    name:data.name,
    address:data.address,
    organisationId:id
    
  }, { transaction });
}; 

const findAllBranch = async (transaction) => {
  return await Branch.findAll({ transaction });
};

const deleteBranch = async (id,transaction) => {
  return await Branch.destroy({
    where:{id:id}, transaction });
};

const updateBranch = async (id,data,transaction) => {
  return await Branch.update({
    name:data.name,
    color:data.color,  
  },{
    where:{
      id:id,
    },transaction
  });
};

const branchById=async(id,trnsaction)=>{
  return await Branch.findByPk(id,{transaction});
}
const branchByOrganisationId=async(id,trnsaction)=>{
  return await Branch.findAll({whre:{
    organisationId:id
  }},{ transaction });
}

export { addBranch, findAllBranch ,deleteBranch,updateBranch,branchById,branchByOrganisationId,addBranchByOrganisationId };      
