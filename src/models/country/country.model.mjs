import Country from "./country.postgres.mjs";



const addCountry = async (data, transaction) => {
  return await Country.create({
    name:data.name,
    address:data.address,
    
  }, { transaction });
}; 

const findAllCountry = async (transaction) => {
  return await Country.findAll({ transaction });
};

const deleteCountry = async (id,transaction) => {
  return await Country.destroy({
    where:{id:id}, transaction });
};

const updateCountry = async (id,data,transaction) => {
  return await Country.update({
    name:data.name,
    color:data.color,  
  },{
    where:{
      id:id,
    },transaction
  });
};

export {addCountry,findAllCountry,deleteCountry,updateCountry}