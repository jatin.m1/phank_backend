import { DataTypes } from "sequelize";
import { sequelize } from "../../services/postgres.mjs";

const Salary = sequelize.define("Salary", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(20),
    allowNull: false,
    validate: {
      notNull: {
        msg: "Please enter test name",
      },
    },
  },

});

export default Salary;
