import Salary from './salary.postgres.mjs'

const addSalary= async (data, transaction) => {
  return  await Salary.create({name:data.name},transaction);
};

const findAllSalary = async (transaction) => {
  return await Salary.findAll({ transaction });
};

const deleteSalary = async (id,transaction) => {
  return await Salary.destroy({
    where:{id:id}, transaction });
};

const updateSalary = async (id,name,address,transaction) => {
  return await Salary.update({
    name:name
  },{
    where:{
      id:id,
    },transaction
  });
};
export { addSalary, findAllSalary ,deleteSalary,updateSalary};      
