import Organisation from './organisation.postgres.mjs'

const addOrganisation= async (data, transaction) => {
  return  await Organisation.create({name:data.name,address:data.address,status:data.status},transaction);
};

const findAllOrganisation = async (transaction) => {
  return await Organisation.findAll({ transaction });
};

const deleteOrganisation = async (id,transaction) => {
  return await Organisation.destroy({
    where:{id:id}, transaction });
};

const updateOrganisation = async (id,name,address,transaction) => {
  return await Organisation.update({
    name:name, 
    address:address
  },{
    where:{
      id:id,
    },transaction
  });
};
export { addOrganisation, findAllOrganisation ,deleteOrganisation,updateOrganisation};      
