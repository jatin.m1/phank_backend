import State from "../../models/state/state.postgres.mjs";
import Country from "../../models/country/country.postgres.mjs";

Country.hasMany(State,{
    foreignKey:"countryId",
    sourceKey:"id",
     onDelete:"RESTRICT"
})