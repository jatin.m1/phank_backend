import Vehical from "../../models/vehical/vehical.postgres.mjs"
import Vmake from "../../models/vehical/vmake.postgres.mjs"
import Vtype from "../../models/vehical/vtype.postgres.mjs"
import Branch from "../../models/branch/branch.postgres.mjs"
import Organisation  from "../../models/organisation/organisation.postgres.mjs"
import Employee from "../../models/employee/employe.postgres.mjs"

Vmake.hasMany(Vehical,{
    foreignKey:"vmake",
    sourceKey:"id",
    onDelete: 'RESTRICT'
})

Vtype.hasMany(Vehical,{
    foreignKey:"vtype",
    sourceKey:"id",
    onDelete: 'RESTRICT'
})

Organisation.hasMany(Vehical,{
    foreignKey:"organisationId",
    sourceKey:"id",
    onDelete: 'RESTRICT'
})


Branch.hasMany(Vehical,{
    foreignKey:"branchId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})

Employee.hasMany(Vehical,{
    foreignKey:"employeeId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})

