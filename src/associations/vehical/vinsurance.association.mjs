import Vehical from "../../models/vehical/vehical.postgres.mjs"
import Vinsurance from "../../models/vehical/vinsurance.postgres.mjs"

Vehical.hasMany(Vinsurance,{
    foreignKey:"vehicalId",
    sourceKey:"id",
    onDelete: 'RESTRICT'

})