import Branch from "../../models/branch/branch.postgres.mjs"
import Status from "../../models/status/status.postgres.mjs"
import State from "../../models/state/state.postgres.mjs"
import Organisation from "../../models/organisation/organisation.postgres.mjs"

// one to many relation      {between status and branch }
Status.hasMany(Branch,{
    foreignKey:"statusId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Branch.belongsTo(Status,{
    foreignKey:"statusId",
    targetKey:"id",
    onDelete:"RESTRICT"
});

State.hasMany(Branch,{
    foreignKey:"stateId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})

Organisation.hasMany(Branch,{
    foreignKey:"organisationId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
