import Relationship from "../../models/employee/relationship.postgres.mjs";
import Status from "../../models/status/status.postgres.mjs";

Status.hasMany(Relationship,{
    foreignKey:"status",
    sourceKey:"id",
    onDelete:"RESTRICT"
})