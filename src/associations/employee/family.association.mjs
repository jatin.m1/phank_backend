import Relationship from "../../models/employee/relationship.postgres.mjs"
import Gender from "../../models/employee/gender.postgres.mjs"
import Occupation from "../../models/employee/occupation.postgres.mjs"
import Employee from "../../models/employee/employe.postgres.mjs"
import Family from "../../models/employee/employee-family.postgres.mjs"

Gender.hasMany(Family,{
    foreignKey:"genderId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})

Occupation.hasMany(Family,{
    foreignKey:"occupationId",
    sourceKey:"id",
    onDelete:"RESTRICT"

})

Employee.hasMany(Family,{
    foreignKey:"employeeId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Relationship.hasMany(Family,{
    foreignKey:"relationshipId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
