import Egrade from "../../models/employee/egrade.postgres.mjs";
import Status from "../../models/status/status.postgres.mjs";
import Organisation from "../../models/organisation/organisation.postgres.mjs";
import Leave from "../../models/employee/leave.postgres.mjs"
import Salary from "../../models/salary/salary.postgres.mjs"

Status.hasMany(Egrade,{
    foreignKey:"status",
    sourceKey:"id",
    onDelete:"RESTRICT"
})


Organisation.hasMany(Egrade,{
    foreignKey:"organisationId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})

Salary.hasMany(Egrade,{
    foreignKey:"salaryStructure",
    sourceKey:"id",
    onDelete:"RESTRICT"
})


Leave.hasMany(Egrade,{
    foreignKey:"leavePolicy",
    sourceKey:"id",
    onDelete:"RESTRICT"
})