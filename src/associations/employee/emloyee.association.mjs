import Employee from "../../models/employee/employe.postgres.mjs";
import Etype from "../../models/employee/etypes.postgres.mjs";
import Gender from "../../models/employee/gender.postgres.mjs";
import Status from "../../models/status/status.postgres.mjs";
import PaymentModes from "../../models/paymentMode/paymentModes.postgres.mjs";
import Manager from "../../models/employee/manager.postgres.mjs";
import Edepartment from "../../models/employee/edepartment.postgres.mjs"
import Branch from "../../models/branch/branch.postgres.mjs"

Gender.hasMany(Employee,{
    foreignKey:"genderId",
    sorceKey:"id",
    onDelete:"RESTRICT"
})
Status.hasMany(Employee,{
    foreignKey:"status"
,sourceKey:"id",
onDelete:"RESTRICT"
})
Etype.hasMany(Employee,{
    foreignKey:"etypeId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
PaymentModes.hasMany(Employee,{
    foreignKey:"preferedpaymentMode",
    sorceKey:"id",
    onDelete:"RESTRICT"
})

Manager.hasMany(Employee,{
    foreignKey:"ManagerId",
    sorceKey:"id",
    onDelete:"RESTRICT"
})
Edepartment.hasMany(Employee,{
    foreignKey:"departmentId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Branch.hasMany(Employee,{
    foreignKey:"branchId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})

