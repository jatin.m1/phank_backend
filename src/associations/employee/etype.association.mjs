import Status from "../../models/status/status.postgres.mjs";
import Etype from "../../models/employee/etypes.postgres.mjs"

Status.hasMany(Etype,{
    foreignKey:"status",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Etype.belongsTo(Status,{
    foreignKey:"status",
    targetKey:"id"
})
