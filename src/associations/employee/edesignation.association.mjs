import Status from "../../models/status/status.postgres.mjs";
import Edesignation from '../../models/employee/edesignation.postgres.mjs'
import Organisation from "../../models/organisation/organisation.postgres.mjs"


Status.hasMany(Edesignation,{
    foreignKey:"status",
    sourceKey:"id",
    onDelete:"RESTRICT"
})

Edesignation.belongsTo(Status,{
    foreignKey:"status",
    targetKey:"id"
})

Organisation.hasMany(Edesignation,{
    foreignKey:"organisationId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})

Edesignation.belongsTo(Organisation,{
    foreignKey:"organisationId",
    targetKey:"id"
})