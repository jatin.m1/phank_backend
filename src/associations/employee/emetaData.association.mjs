import Edepartment from "../../models/employee/edepartment.postgres.mjs"
import Emetadata from "../../models/employee/emetaData.postgres.mjs"
import Egrade from "../../models/employee/egrade.postgres.mjs"
import Edesignation from "../../models/employee/edesignation.postgres.mjs"
import Branch from  "../../models/branch/branch.postgres.mjs"
import Employee from "../../models/employee/employe.postgres.mjs"



Edepartment.hasMany(Emetadata,{
    foreignKey:"edepartmentId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Egrade.hasMany(Emetadata,{
    foreignKey:"egradeId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Edesignation.hasMany(Emetadata,{
    foreignKey:"edesignationId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Branch.hasMany(Emetadata,{
    foreignKey:"BranchId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Employee.hasMany(Emetadata,{
    foreignKey:"employeeId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
