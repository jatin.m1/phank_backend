import Employee from "../../models/employee/employe.postgres.mjs"
import Qualification from "../../models/employee/qualification.postgres.mjs"
import Level from "../../models/employee/level.postgres.mjs"
import Eeducation from "../../models/employee/eeducation.postgres.mjs"

Employee.hasOne(Eeducation,{
    foreignKey:"employeeId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})

Level.hasMany(Eeducation,{
    foreignKey:"levelId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Qualification.hasOne(Eeducation,{
foreignKey:"qualificationId",
sourceKey:"id",
onDelete:"RESTRICT"
})
