import Status from "../../models/status/status.postgres.mjs";
import Edepartment from '../../models/employee/edepartment.postgres.mjs'
import Organisation from "../../models/organisation/organisation.postgres.mjs"


Status.hasMany(Edepartment,{
    foreignKey:"status",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Edepartment.belongsTo(Status,{
    foreignKey:"status",
    targetKey:"id",
});

Organisation.hasMany(Edepartment,{
    foreignKey:"organisationId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})
Edepartment.belongsTo(Organisation,{
    foreignKey:"organisationId",
    targetKey:"id",
});