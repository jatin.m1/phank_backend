import Status from "../../models/status/status.postgres.mjs";
import Organisation from "../../models/organisation/organisation.postgres.mjs"

Status.hasMany(Organisation,{
    foreignKey:"status",
    sourceKey:"id",
    onDelete:"RESTRICT"
})