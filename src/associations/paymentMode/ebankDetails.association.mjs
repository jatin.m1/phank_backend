import Bank from "../../models/paymentMode/bank.postgres.mjs"
import EbankDetails from "../../models/paymentMode/ebankDetails.postgres.mjs"
import Employee from "../../models/employee/employe.postgres.mjs"

Bank.hasMany(EbankDetails,{
    foreignKey:"bankId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})

Employee.hasMany(EbankDetails,{
    foreignKey:"employeeId",
    sourceKey:"id",
    onDelete:"RESTRICT"
})